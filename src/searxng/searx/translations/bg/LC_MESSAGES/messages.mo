��    p      �                        /     5     =  E   J     �  !   �     �  (   �     �                    5     H     Y     e  ]   n     �     �     �     �     �     	     	     	     	     .	     E	  	   M	     W	  	   [	     e	     u	     �	  .   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     $
     (
     1
  %   4
     Z
  o   i
     �
     �
  
   �
  
   �
     
          (     B     Q     X     u     ~     �     �     �     �     �  &   �  S   �  [   <     �     �     �  
   �  '   �     �     �     �  (   �          "     *  
   3  
   >     I     P     S     W     c     g     m  	   r     |     �     �     �     �     �     �  
   �  
   �     �     �     �     �  R     (   U     ~  �  �  +   \     �     �  )   �  U   �  
   (  7   3  6   k  P   �     �  "        %  7   8  +   p     �     �     �  e   �  !   ?     a     r  .   �     �     �     �     �  '   	  $   1     V  !   _     �     �  %   �     �     �  i   �     S     k     x     �  
   �     �     �     �     �     �     �          !     ?  5   P  .   �  �   �     c  '   ~  !   �  "   �     �     �  4     *   P     {  <   �     �     �     �          #     4     =  >   T  �   �  �         �     �          +  =   7     u  
   �     �  =   �     �     �     �          &     @     W     Z     k  
   �     �     �  !   �     �  !   �       
     &   %     L     h     �     �     �     �     �  
   �  �     G   �  /   �   Advanced settings Allow Answers Autocomplete Automatically load next page when scrolling to bottom of current page Bytes Change the language of the layout Choose style for this theme Click on the magnifier to perform search Close Cookie name Cookies Currently used search engines Default categories Default language Description Disabled Displays your IP if the query is "ip" and your user agent if the query contains "user agent". Download results Enabled Engine name Engine stats Engines Error! Examples Filesize Filter content Find stuff as you type General Get image GiB Heads up! Infinite scroll Information! Interface language Invalid settings, please edit your preferences Keywords Leecher Links Max time Method MiB Moderate Name None Number of Files Number of results Off Oh snap! On Open result links on new browser tabs Page not found Perform search immediately if a category selected. Disable to select multiple categories. (JavaScript required) Preferences Reset defaults SafeSearch Search URL Search for... Search language Search on category select Search results Seeder Settings saved successfully. Shortcut Something went wrong. Sorry! Start search Strict Style Suggestions There is currently no data available.  These cookies serve your sole convenience, we don't use these cookies to track you. These settings are stored in your cookies, this allows us not to store this data about you. TiB Value View source Well done! What language do you prefer for search? about back cached currently, there are no cookies defined. files general hide map hide media hide video images it kiB magnet link map music news next page preferences previous page save science search error search page show map show media show video social media stats torrent file videos we didn't find any results. Please use another query or search in more categories. {hours} hour(s), {minutes} minute(s) ago {minutes} minute(s) ago Project-Id-Version:  searx
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2021-10-28 06:54+0000
PO-Revision-Date: 2021-11-03 08:06+0000
Last-Translator: Markus Heiser <markus.heiser@darmarit.de>
Language: bg
Language-Team: Bulgarian <https://weblate.bubu1.eu/projects/searxng/searxng/bg/>
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.9.1
 Допълнителни настройки Позволи Отговори Автоматично допълване Автоматично зареждане на следващата страница. Байта Промени езика на оформлението Избери стил за избрания облик Кликнете лупичката, за да изпълните търсене Затвори Име на бисквитката Бисквитки Използвани търсачки в момента Първоначални категории Търси на език Описание Изключено Показва IP-то ви и др. инфо, ако търсенето е "ip" или "user agent". Свали резултатите Включено Име на търсачка Статистика на търсачката Търсачки Грешка! Примери Размер на файла Филтрирай съдържание Намери докато пишеш Общи Вземи изображение гигабайт Внимание! Списък без страници. Информация! Език Неправилни настройки, моля проверете предпочитанията си. Ключови думи Лийчър Връзки Макс. време Метод мегабайт Умерено Име Нищо Брой на Файлове Брой резултати Изключено Да му се не види! Включено Отвори връзките в нов раздел. Страницата не е намерена. Търси веднага при избрана категория. Изключи за избор на няколко категории. (Необходим е JavaScript) Предпочитания Върни първоначалните Безопасно търсене Адрес на търсенето Търси за... Език на търсене Търси при избор на категория Резултати от търсенето Сийдър Настройките са успешно запазени. Пряк път Нещо се обърка. Съжалявам! Започни търсене Стриктно Стил Предложения Няма налична достъпна информация. Тези бисквитки служат за ваше удобство. Ние не ги използваме, за да ви следим. Тези настройки се съхраняват във вашите бисквитки. Това ни позволява да не съхраняваме тази информация за вас. терабайт Стойност Покажи източник Браво! Кой език предпочитате за търсене? относно назад кеширана В момента няма налични бисквитки. файлове общо скрий картата скрий медия скрий видеото изображения IT килобайт магнитна връзка карта музика новини следваща страница предпочитания предишна страница запази наука грешка при търсенето търси страница покажи карта покажи медия покажи видео социална мрежа статистики торент файл видео не намерихме резултати. Моля пробвайте други ключови думи или търсете в повече категории. преди {hours} час(ове), {minutes} минута(минути) преди {minutes} минута(минути) 