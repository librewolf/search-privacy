# Norwegian Bokmål (Norway) translations for PROJECT.
# Copyright (C) 2021 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2021-10-28 06:54+0000\n"
"PO-Revision-Date: 2021-11-01 23:48+0000\n"
"Last-Translator: Markus Heiser <markus.heiser@darmarit.de>\n"
"Language-Team: Norwegian Bokmål <https://weblate.bubu1.eu/projects/searxng/"
"searxng/nb_NO/>\n"
"Language: nb_NO\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.8.1\n"
"Generated-By: Babel 2.9.1\n"

#: searx/webapp.py:164
msgid "files"
msgstr "filer"

#: searx/webapp.py:165
msgid "general"
msgstr "generelt"

#: searx/webapp.py:166
msgid "music"
msgstr "musikk"

#: searx/webapp.py:167
msgid "social media"
msgstr "sosiale media"

#: searx/webapp.py:168
msgid "images"
msgstr "bilder"

#: searx/webapp.py:169
msgid "videos"
msgstr "videoer"

#: searx/webapp.py:170
msgid "it"
msgstr "IT"

#: searx/webapp.py:171
msgid "news"
msgstr "nyheter"

#: searx/webapp.py:172
msgid "map"
msgstr "kart"

#: searx/webapp.py:173
msgid "onions"
msgstr "løktjenester"

#: searx/webapp.py:174
msgid "science"
msgstr "vitenskap"

#: searx/webapp.py:178
msgid "timeout"
msgstr "tidsavbrudd"

#: searx/webapp.py:179
msgid "parsing error"
msgstr "tolkningsfeil"

#: searx/webapp.py:180
msgid "HTTP protocol error"
msgstr "HTTP-protokollfeil"

#: searx/webapp.py:181
msgid "network error"
msgstr "nettverksfeil"

#: searx/webapp.py:183
msgid "unexpected crash"
msgstr "uventet krasj"

#: searx/webapp.py:190
msgid "HTTP error"
msgstr "HTTP-feil"

#: searx/webapp.py:191
msgid "HTTP connection error"
msgstr "HTTP-tilkoblingsfeil"

#: searx/webapp.py:197
msgid "proxy error"
msgstr "mellomtjenerfeil"

#: searx/webapp.py:198
msgid "CAPTCHA"
msgstr "CAPTCHA"

#: searx/webapp.py:199
msgid "too many requests"
msgstr "for mange forespørsler"

#: searx/webapp.py:200
msgid "access denied"
msgstr "tilgang nektet"

#: searx/webapp.py:201
msgid "server API error"
msgstr "Tjener-API-feil"

#: searx/webapp.py:393
msgid "No item found"
msgstr "Fant ingen elementer"

#: searx/engines/qwant.py:198
#: searx/templates/simple/result_templates/images.html:23 searx/webapp.py:395
msgid "Source"
msgstr "Kilde"

#: searx/webapp.py:505 searx/webapp.py:912
msgid "Invalid settings, please edit your preferences"
msgstr "Ugyldige innstillinger. Rediger dine preferanser."

#: searx/webapp.py:521
msgid "Invalid settings"
msgstr "Ugyldige innstillinger"

#: searx/webapp.py:599 searx/webapp.py:665
msgid "search error"
msgstr "søkefeil"

#: searx/webapp.py:708
msgid "{minutes} minute(s) ago"
msgstr "for {minutes} minute(s) siden"

#: searx/webapp.py:710
msgid "{hours} hour(s), {minutes} minute(s) ago"
msgstr "for {hours} time(r), {minutes} minutt(er) siden"

#: searx/webapp.py:833
msgid "Suspended"
msgstr "I hvilemodus"

#: searx/answerers/random/answerer.py:65
msgid "Random value generator"
msgstr "Generator for tilfeldige tall"

#: searx/answerers/random/answerer.py:66
msgid "Generate different random values"
msgstr "Generer forskjellige tilfeldige verdier"

#: searx/answerers/statistics/answerer.py:50
msgid "Statistics functions"
msgstr "Statistikkfunksjoner"

#: searx/answerers/statistics/answerer.py:51
msgid "Compute {functions} of the arguments"
msgstr "Regn ut {functions} av parameterne"

#: searx/engines/openstreetmap.py:155
msgid "Get directions"
msgstr "Få veibeskrivelser"

#: searx/engines/pdbe.py:90
msgid "{title} (OBSOLETE)"
msgstr "{title} (FORELDET)"

#: searx/engines/pdbe.py:97
msgid "This entry has been superseded by"
msgstr "Denne oppføringen har blitt erstattet av"

#: searx/engines/pubmed.py:78
msgid "No abstract is available for this publication."
msgstr "Sammendrag er ikke tilgjengelig for denne publikasjonen."

#: searx/engines/qwant.py:200
msgid "Channel"
msgstr "Kanal"

#: searx/plugins/hash_plugin.py:24
msgid "Converts strings to different hash digests."
msgstr "Konverterer strenger til andre sjekksumsføljetonger."

#: searx/plugins/hash_plugin.py:52
msgid "hash digest"
msgstr "sjekksumsføljetong"

#: searx/plugins/hostname_replace.py:9
msgid "Hostname replace"
msgstr ""

#: searx/plugins/hostname_replace.py:10
msgid "Rewrite result hostnames or remove results based on the hostname"
msgstr ""

#: searx/plugins/infinite_scroll.py:3
msgid "Infinite scroll"
msgstr "Uendelig rulling"

#: searx/plugins/infinite_scroll.py:4
msgid "Automatically load next page when scrolling to bottom of current page"
msgstr "Last inn neste side automatisk ved rulling til bunnen av nåværende side"

#: searx/plugins/oa_doi_rewrite.py:9
msgid "Open Access DOI rewrite"
msgstr "Open Access DOI-omskriving"

#: searx/plugins/oa_doi_rewrite.py:10
msgid ""
"Avoid paywalls by redirecting to open-access versions of publications "
"when available"
msgstr ""
"Tillat betalingsmurer ved å videresende til åpen-tilgang -versjoner av "
"publikasjoner når de forefinnes"

#: searx/plugins/search_on_category_select.py:18
msgid "Search on category select"
msgstr "Søk ved kategorivalg"

#: searx/plugins/search_on_category_select.py:19
msgid ""
"Perform search immediately if a category selected. Disable to select "
"multiple categories. (JavaScript required)"
msgstr ""
"Utfør søk umiddelbart når en kategori velges. Skru av for å velge flere "
"kategorier. (JavaScript kreves)"

#: searx/plugins/self_info.py:19
msgid "Self Informations"
msgstr "Selv-informasjon"

#: searx/plugins/self_info.py:20
msgid ""
"Displays your IP if the query is \"ip\" and your user agent if the query "
"contains \"user agent\"."
msgstr ""
"Viser din IP hvis spørringen er \"ip\" og din brukeragent hvis spørringen "
"inneholder \"user agent\"."

#: searx/plugins/tracker_url_remover.py:27
msgid "Tracker URL remover"
msgstr "Sporings-nettadressefjerner"

#: searx/plugins/tracker_url_remover.py:28
msgid "Remove trackers arguments from the returned URL"
msgstr "Fjern sporer-argumenter fra returnert nettadresse"

#: searx/plugins/vim_hotkeys.py:3
msgid "Vim-like hotkeys"
msgstr "Vim-lignende hurtigtaster"

#: searx/plugins/vim_hotkeys.py:4
msgid ""
"Navigate search results with Vim-like hotkeys (JavaScript required). "
"Press \"h\" key on main or result page to get help."
msgstr ""
"Naviger søkeresultater med Vim-lignende hurtigtaster (JavaScript kreves). "
"Trykk \"h\"-tasten på hoved- eller resultatsiden for å få hjelp."

#: searx/templates/oscar/404.html:4 searx/templates/simple/404.html:4
msgid "Page not found"
msgstr "Fant ikke siden"

#: searx/templates/oscar/404.html:6 searx/templates/simple/404.html:6
#, python-format
msgid "Go to %(search_page)s."
msgstr "Gå til %(search_page)s."

#: searx/templates/oscar/404.html:6 searx/templates/simple/404.html:6
msgid "search page"
msgstr "søkeside"

#: searx/templates/oscar/about.html:2 searx/templates/oscar/navbar.html:6
msgid "about"
msgstr "om"

#: searx/templates/oscar/advanced.html:4
msgid "Advanced settings"
msgstr "Avanserte innstillinger"

#: searx/templates/oscar/base.html:55
#: searx/templates/oscar/messages/first_time.html:4
#: searx/templates/oscar/messages/save_settings_successfull.html:5
#: searx/templates/oscar/messages/unknow_error.html:5
msgid "Close"
msgstr "Lukk"

#: searx/templates/oscar/base.html:57
#: searx/templates/oscar/messages/no_results.html:4
#: searx/templates/simple/messages/no_results.html:4
#: searx/templates/simple/results.html:45
msgid "Error!"
msgstr "Feil!"

#: searx/templates/oscar/base.html:85 searx/templates/simple/base.html:53
msgid "Powered by"
msgstr "Drevet av"

#: searx/templates/oscar/base.html:85 searx/templates/simple/base.html:53
msgid "a privacy-respecting, hackable metasearch engine"
msgstr "en personvernsrespekterende, hackbar metasøkemotor"

#: searx/templates/oscar/base.html:86 searx/templates/simple/base.html:54
msgid "Source code"
msgstr "Kildekode"

#: searx/templates/oscar/base.html:87 searx/templates/simple/base.html:55
msgid "Issue tracker"
msgstr "Problemsporer"

#: searx/templates/oscar/base.html:88 searx/templates/oscar/stats.html:18
#: searx/templates/simple/base.html:56 searx/templates/simple/stats.html:21
msgid "Engine stats"
msgstr "Søkemotorstatistikk"

#: searx/templates/oscar/base.html:89
#: searx/templates/oscar/messages/no_results.html:13
#: searx/templates/simple/base.html:57
#: searx/templates/simple/messages/no_results.html:14
msgid "Public instances"
msgstr "Offentlige instanser"

#: searx/templates/oscar/base.html:90 searx/templates/simple/base.html:58
msgid "Contact instance maintainer"
msgstr "Kontakt tilbyderen av instansen"

#: searx/templates/oscar/languages.html:2
msgid "Language"
msgstr "Språk"

#: searx/templates/oscar/languages.html:4
#: searx/templates/simple/languages.html:2
#: searx/templates/simple/preferences.html:119
msgid "Default language"
msgstr "Forvalgt språk"

#: searx/templates/oscar/macros.html:23
#: searx/templates/simple/result_templates/torrent.html:6
msgid "magnet link"
msgstr "magnetlenke"

#: searx/templates/oscar/macros.html:24
#: searx/templates/simple/result_templates/torrent.html:7
msgid "torrent file"
msgstr "torrentfil"

#: searx/templates/oscar/macros.html:37 searx/templates/oscar/macros.html:39
#: searx/templates/oscar/macros.html:73 searx/templates/oscar/macros.html:75
#: searx/templates/simple/macros.html:46
msgid "cached"
msgstr "hurtiglagret"

#: searx/templates/oscar/macros.html:43 searx/templates/oscar/macros.html:59
#: searx/templates/oscar/macros.html:79 searx/templates/oscar/macros.html:93
#: searx/templates/simple/macros.html:46
msgid "proxied"
msgstr "mellomtjent"

#: searx/templates/oscar/macros.html:133
#: searx/templates/oscar/preferences.html:319
#: searx/templates/oscar/preferences.html:337
#: searx/templates/oscar/preferences.html:391
#: searx/templates/simple/preferences.html:251
#: searx/templates/simple/preferences.html:289
msgid "Allow"
msgstr "Tillat"

#: searx/templates/oscar/macros.html:139
msgid "broken"
msgstr "knekt"

#: searx/templates/oscar/macros.html:141
msgid "supported"
msgstr "støttet"

#: searx/templates/oscar/macros.html:143
msgid "not supported"
msgstr "ikke støttet"

#: searx/templates/oscar/navbar.html:7
#: searx/templates/oscar/preferences.html:90
msgid "preferences"
msgstr "innstillinger"

#: searx/templates/oscar/preferences.html:12
#: searx/templates/simple/preferences.html:28
msgid "No HTTPS"
msgstr "Ingen HTTPS"

#: searx/templates/oscar/preferences.html:14
#: searx/templates/oscar/results.html:27 searx/templates/simple/results.html:40
msgid "Number of results"
msgstr "Antall resultater"

#: searx/templates/oscar/preferences.html:14
msgid "Avg."
msgstr "Gjen."

#: searx/templates/oscar/messages/no_results.html:8
#: searx/templates/oscar/preferences.html:17
#: searx/templates/oscar/preferences.html:18
#: searx/templates/oscar/results.html:36
#: searx/templates/simple/messages/no_results.html:8
#: searx/templates/simple/preferences.html:30
#: searx/templates/simple/preferences.html:31
#: searx/templates/simple/results.html:48
msgid "View error logs and submit a bug report"
msgstr "Vis feillogger og send inn en feilrapport"

#: searx/templates/oscar/preferences.html:38
#: searx/templates/oscar/stats.html:70
#: searx/templates/simple/preferences.html:52
#: searx/templates/simple/stats.html:70
msgid "Median"
msgstr "Median"

#: searx/templates/oscar/preferences.html:39
#: searx/templates/oscar/stats.html:76
#: searx/templates/simple/preferences.html:53
#: searx/templates/simple/stats.html:76
msgid "P80"
msgstr "P80"

#: searx/templates/oscar/preferences.html:40
#: searx/templates/oscar/stats.html:82
#: searx/templates/simple/preferences.html:54
#: searx/templates/simple/stats.html:82
msgid "P95"
msgstr "P95"

#: searx/templates/oscar/preferences.html:68
#: searx/templates/simple/preferences.html:82
msgid "Failed checker test(s): "
msgstr "Mislykket/ede sjekkingstest(er): "

#: searx/templates/oscar/preferences.html:96
#: searx/templates/simple/preferences.html:100
msgid "Preferences"
msgstr "Innstillinger"

#: searx/templates/oscar/preferences.html:101
#: searx/templates/oscar/preferences.html:111
#: searx/templates/simple/preferences.html:106
msgid "General"
msgstr "Generelt"

#: searx/templates/oscar/preferences.html:102
#: searx/templates/oscar/preferences.html:193
msgid "User Interface"
msgstr "Brukergrensesnitt"

#: searx/templates/oscar/preferences.html:103
#: searx/templates/oscar/preferences.html:257
#: searx/templates/simple/preferences.html:215
msgid "Privacy"
msgstr "Personvern"

#: searx/templates/oscar/preferences.html:104
#: searx/templates/oscar/preferences.html:296
#: searx/templates/simple/preferences.html:243
msgid "Engines"
msgstr "Søkemotorer"

#: searx/templates/oscar/preferences.html:105
#: searx/templates/simple/preferences.html:284
msgid "Special Queries"
msgstr "Spesialspørringer"

#: searx/templates/oscar/preferences.html:106
#: searx/templates/oscar/preferences.html:431
#: searx/templates/simple/preferences.html:324
msgid "Cookies"
msgstr "Kaker"

#: searx/templates/oscar/preferences.html:123
#: searx/templates/oscar/preferences.html:125
#: searx/templates/simple/preferences.html:109
msgid "Default categories"
msgstr "Forvalgte kategorier"

#: searx/templates/oscar/preferences.html:133
#: searx/templates/simple/preferences.html:116
#: searx/templates/simple/preferences.html:225
msgid "Search language"
msgstr "Søkespråk"

#: searx/templates/oscar/preferences.html:134
#: searx/templates/simple/preferences.html:125
msgid "What language do you prefer for search?"
msgstr "Hvilket språk foretrekker du for søk?"

#: searx/templates/oscar/preferences.html:141
#: searx/templates/oscar/preferences.html:323
#: searx/templates/oscar/preferences.html:333
#: searx/templates/simple/preferences.html:144
#: searx/templates/simple/preferences.html:255
msgid "SafeSearch"
msgstr "TrygtSøk"

#: searx/templates/oscar/preferences.html:142
#: searx/templates/simple/preferences.html:152
msgid "Filter content"
msgstr "Filtrer innhold"

#: searx/templates/oscar/preferences.html:145
#: searx/templates/simple/preferences.html:147
msgid "Strict"
msgstr "Strengt"

#: searx/templates/oscar/preferences.html:146
#: searx/templates/simple/preferences.html:148
msgid "Moderate"
msgstr "Moderat"

#: searx/templates/oscar/preferences.html:147
#: searx/templates/simple/preferences.html:149
msgid "None"
msgstr "Ingen"

#: searx/templates/oscar/preferences.html:153
#: searx/templates/simple/preferences.html:130
msgid "Autocomplete"
msgstr "Auto-fullføring"

#: searx/templates/oscar/preferences.html:154
#: searx/templates/simple/preferences.html:139
msgid "Find stuff as you type"
msgstr "Finn ting mens du skriver"

#: searx/templates/oscar/preferences.html:168
#: searx/templates/simple/preferences.html:158
msgid "Open Access DOI resolver"
msgstr "Open Access DOI-utleder"

#: searx/templates/oscar/preferences.html:169
#: searx/templates/simple/preferences.html:168
msgid ""
"Redirect to open-access versions of publications when available (plugin "
"required)"
msgstr ""
"Videresend til åpen tilgang-versjoner av publikasjoner når de finnes "
"(programtillegg kreves)"

#: searx/templates/oscar/preferences.html:183
msgid "Engine tokens"
msgstr "Søkemotorsymboler"

#: searx/templates/oscar/preferences.html:184
msgid "Access tokens for private engines"
msgstr "Tilgangssymboler for private motorer"

#: searx/templates/oscar/preferences.html:198
#: searx/templates/simple/preferences.html:176
msgid "Interface language"
msgstr "Grensesnitts-språk"

#: searx/templates/oscar/preferences.html:199
#: searx/templates/simple/preferences.html:184
msgid "Change the language of the layout"
msgstr "Endre språket for oppsettet"

#: searx/templates/oscar/preferences.html:210
#: searx/templates/simple/preferences.html:189
msgid "Theme"
msgstr ""

#: searx/templates/oscar/preferences.html:211
#: searx/templates/simple/preferences.html:197
msgid "Change SearXNG layout"
msgstr ""

#: searx/templates/oscar/preferences.html:222
#: searx/templates/oscar/preferences.html:228
msgid "Choose style for this theme"
msgstr "Velg stil for denne drakten"

#: searx/templates/oscar/preferences.html:222
#: searx/templates/oscar/preferences.html:228
msgid "Style"
msgstr "Stil"

#: searx/templates/oscar/preferences.html:231
msgid "Show advanced settings"
msgstr "Vis avanserte innstillinger"

#: searx/templates/oscar/preferences.html:232
msgid "Show advanced settings panel in the home page by default"
msgstr "Vis panel for avanserte innstillinger på hjemmesiden som forvalg"

#: searx/templates/oscar/preferences.html:235
#: searx/templates/oscar/preferences.html:245
#: searx/templates/simple/preferences.html:205
msgid "On"
msgstr "På"

#: searx/templates/oscar/preferences.html:236
#: searx/templates/oscar/preferences.html:246
#: searx/templates/simple/preferences.html:206
msgid "Off"
msgstr "Av"

#: searx/templates/oscar/preferences.html:241
#: searx/templates/simple/preferences.html:202
msgid "Results on new tabs"
msgstr "Resultater i nye faner"

#: searx/templates/oscar/preferences.html:242
#: searx/templates/simple/preferences.html:209
msgid "Open result links on new browser tabs"
msgstr "Åpne resultatlenker i nye nettleserfaner"

#: searx/templates/oscar/preferences.html:262
#: searx/templates/simple/preferences.html:218
msgid "Method"
msgstr "Metode"

#: searx/templates/oscar/preferences.html:263
msgid ""
"Change how forms are submited, <a "
"href=\"http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods\""
" rel=\"external\">learn more about request methods</a>"
msgstr ""
"Endre hvordan skjemaer innsendes, <a "
"href=\"http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods\""
" rel=\"external\">lær mer om forespørselsmetoder</a>"

#: searx/templates/oscar/preferences.html:273
#: searx/templates/simple/preferences.html:230
msgid "Image proxy"
msgstr "Bildemellomtjener"

#: searx/templates/oscar/preferences.html:274
#: searx/templates/simple/preferences.html:237
msgid "Proxying image results through SearXNG"
msgstr ""

#: searx/templates/oscar/preferences.html:277
#: searx/templates/simple/preferences.html:233
msgid "Enabled"
msgstr "Påskrudd"

#: searx/templates/oscar/preferences.html:278
#: searx/templates/simple/preferences.html:234
msgid "Disabled"
msgstr "Avskrudd"

#: searx/templates/oscar/preferences.html:304
msgid "Allow all"
msgstr "Tillat alle"

#: searx/templates/oscar/preferences.html:305
msgid "Disable all"
msgstr "Nekt alle"

#: searx/templates/oscar/preferences.html:320
#: searx/templates/oscar/preferences.html:336
#: searx/templates/oscar/stats.html:29
#: searx/templates/simple/preferences.html:252
#: searx/templates/simple/stats.html:28
msgid "Engine name"
msgstr "Søkemotornavn"

#: searx/templates/oscar/preferences.html:321
#: searx/templates/oscar/preferences.html:335
#: searx/templates/simple/preferences.html:253
msgid "Shortcut"
msgstr "Snarvei"

#: searx/templates/oscar/preferences.html:322
#: searx/templates/oscar/preferences.html:334
msgid "Selected language"
msgstr "Valgt språk"

#: searx/templates/oscar/preferences.html:324
#: searx/templates/oscar/preferences.html:332
#: searx/templates/oscar/time-range.html:2
#: searx/templates/simple/preferences.html:256
msgid "Time range"
msgstr "Tidsområde"

#: searx/templates/oscar/preferences.html:325
#: searx/templates/oscar/preferences.html:331
#: searx/templates/oscar/stats.html:32
#: searx/templates/simple/preferences.html:257
#: searx/templates/simple/stats.html:31
msgid "Response time"
msgstr "Svartid"

#: searx/templates/oscar/preferences.html:326
#: searx/templates/oscar/preferences.html:330
#: searx/templates/simple/preferences.html:258
msgid "Max time"
msgstr "Mak. tid"

#: searx/templates/oscar/preferences.html:327
#: searx/templates/oscar/preferences.html:329
#: searx/templates/oscar/stats.html:33
#: searx/templates/simple/preferences.html:259
#: searx/templates/simple/stats.html:32
msgid "Reliability"
msgstr "Pålitelighet"

#: searx/templates/oscar/preferences.html:385
msgid "Query"
msgstr "Spørring"

#: searx/templates/oscar/preferences.html:392
#: searx/templates/simple/preferences.html:290
msgid "Keywords"
msgstr "Nøkkelord"

#: searx/templates/oscar/preferences.html:393
#: searx/templates/simple/preferences.html:291
msgid "Name"
msgstr "Navn"

#: searx/templates/oscar/preferences.html:394
#: searx/templates/simple/preferences.html:292
msgid "Description"
msgstr "Beskrivelse"

#: searx/templates/oscar/preferences.html:395
#: searx/templates/simple/preferences.html:293
msgid "Examples"
msgstr "Eksempler"

#: searx/templates/oscar/preferences.html:400
#: searx/templates/simple/preferences.html:296
msgid "This is the list of SearXNG's instant answering modules."
msgstr ""

#: searx/templates/oscar/preferences.html:413
#: searx/templates/simple/preferences.html:307
msgid "This is the list of plugins."
msgstr "Dette er en liste over programtillegg."

#: searx/templates/oscar/preferences.html:434
#: searx/templates/simple/preferences.html:326
msgid ""
"This is the list of cookies and their values SearXNG is storing on your "
"computer."
msgstr ""

#: searx/templates/oscar/preferences.html:435
#: searx/templates/simple/preferences.html:327
msgid "With that list, you can assess SearXNG transparency."
msgstr ""

#: searx/templates/oscar/preferences.html:440
#: searx/templates/simple/preferences.html:332
msgid "Cookie name"
msgstr "Kakenavn"

#: searx/templates/oscar/preferences.html:441
#: searx/templates/simple/preferences.html:333
msgid "Value"
msgstr "Verdi"

#: searx/templates/oscar/preferences.html:458
#: searx/templates/simple/preferences.html:354
msgid ""
"These settings are stored in your cookies, this allows us not to store "
"this data about you."
msgstr ""
"Disse innstillingene lagres i informasjonskapslene dine, noe som tillater"
" oss å ikke lagre denne dataen om deg."

#: searx/templates/oscar/preferences.html:459
#: searx/templates/simple/preferences.html:356
msgid ""
"These cookies serve your sole convenience, we don't use these cookies to "
"track you."
msgstr ""
"Disse informasjonskapslene er kun til din nytte, de brukes ikke til å "
"spore deg."

#: searx/templates/oscar/preferences.html:463
#: searx/templates/simple/preferences.html:345
msgid "Search URL of the currently saved preferences"
msgstr "Søkenettadresse med nåværende lagrede innstillinger"

#: searx/templates/oscar/preferences.html:464
#: searx/templates/simple/preferences.html:349
msgid ""
"Note: specifying custom settings in the search URL can reduce privacy by "
"leaking data to the clicked result sites."
msgstr ""
"Merk: Å angi egendefinerte innstillinger i søkenettadressen kan redusere "
"personvernet ved å lekke data til sidene det klikkes på i resultatet."

#: searx/templates/oscar/preferences.html:469
#: searx/templates/simple/preferences.html:359
msgid "save"
msgstr "lagre"

#: searx/templates/oscar/preferences.html:470
#: searx/templates/simple/preferences.html:361
msgid "back"
msgstr "tilbake"

#: searx/templates/oscar/preferences.html:471
#: searx/templates/simple/preferences.html:360
msgid "Reset defaults"
msgstr "Tilbakestill forvalg"

#: searx/templates/oscar/results.html:32 searx/templates/simple/results.html:45
msgid "Engines cannot retrieve results"
msgstr "Søkemotorer kan ikke motta resultater"

#: searx/templates/oscar/results.html:53 searx/templates/simple/results.html:66
msgid "Suggestions"
msgstr "Forslag"

#: searx/templates/oscar/results.html:74
msgid "Links"
msgstr "Lenker"

#: searx/templates/oscar/results.html:79 searx/templates/simple/results.html:84
msgid "Search URL"
msgstr "Søkenettadresse"

#: searx/templates/oscar/results.html:84 searx/templates/simple/results.html:89
msgid "Download results"
msgstr "Last ned resultater"

#: searx/templates/oscar/results.html:95
msgid "RSS subscription"
msgstr "RSS-abonnement"

#: searx/templates/oscar/results.html:104
msgid "Search results"
msgstr "Søkeresultater"

#: searx/templates/oscar/results.html:109
#: searx/templates/simple/results.html:113
msgid "Try searching for:"
msgstr "Prøv å søke etter:"

#: searx/templates/oscar/results.html:162
#: searx/templates/oscar/results.html:187
#: searx/templates/simple/results.html:179
msgid "next page"
msgstr "neste side"

#: searx/templates/oscar/results.html:169
#: searx/templates/oscar/results.html:180
#: searx/templates/simple/results.html:162
msgid "previous page"
msgstr "forrige side"

#: searx/templates/oscar/search.html:6 searx/templates/oscar/search_full.html:9
#: searx/templates/simple/search.html:7
#: searx/templates/simple/simple_search.html:4
msgid "Search for..."
msgstr "Søk etter …"

#: searx/templates/oscar/search.html:8
#: searx/templates/oscar/search_full.html:11
#: searx/templates/simple/search.html:9
#: searx/templates/simple/simple_search.html:6
msgid "Start search"
msgstr "Start søk"

#: searx/templates/oscar/search.html:9
#: searx/templates/oscar/search_full.html:12
#: searx/templates/simple/search.html:8
#: searx/templates/simple/simple_search.html:5
msgid "Clear search"
msgstr "Tøm søk"

#: searx/templates/oscar/search_full.html:12
msgid "Clear"
msgstr "Tøm"

#: searx/templates/oscar/stats.html:4
msgid "stats"
msgstr "statistikk"

#: searx/templates/oscar/stats.html:30 searx/templates/simple/stats.html:29
msgid "Scores"
msgstr "Poengsummer"

#: searx/templates/oscar/stats.html:31 searx/templates/simple/stats.html:30
msgid "Result count"
msgstr "Antall resultater"

#: searx/templates/oscar/stats.html:42 searx/templates/simple/stats.html:41
msgid "Scores per result"
msgstr "Vektninger per resultat"

#: searx/templates/oscar/stats.html:65 searx/templates/simple/stats.html:65
msgid "Total"
msgstr "Totalt"

#: searx/templates/oscar/stats.html:66 searx/templates/simple/stats.html:66
msgid "HTTP"
msgstr "HTTP"

#: searx/templates/oscar/stats.html:67 searx/templates/simple/stats.html:67
msgid "Processing"
msgstr "Behandler …"

#: searx/templates/oscar/stats.html:106 searx/templates/simple/stats.html:105
msgid "Warnings"
msgstr "Advarsler"

#: searx/templates/oscar/stats.html:106 searx/templates/simple/stats.html:105
msgid "Errors and exceptions"
msgstr "Feil og unntag"

#: searx/templates/oscar/stats.html:112 searx/templates/simple/stats.html:111
msgid "Exception"
msgstr "Unntak"

#: searx/templates/oscar/stats.html:114 searx/templates/simple/stats.html:113
msgid "Message"
msgstr "Melding"

#: searx/templates/oscar/stats.html:116 searx/templates/simple/stats.html:115
msgid "Percentage"
msgstr "Prosentsats"

#: searx/templates/oscar/stats.html:118 searx/templates/simple/stats.html:117
msgid "Parameter"
msgstr "Parameter"

#: searx/templates/oscar/result_templates/files.html:35
#: searx/templates/oscar/stats.html:126 searx/templates/simple/stats.html:125
msgid "Filename"
msgstr "Filnavn"

#: searx/templates/oscar/stats.html:127 searx/templates/simple/stats.html:126
msgid "Function"
msgstr "Funksjon"

#: searx/templates/oscar/stats.html:128 searx/templates/simple/stats.html:127
msgid "Code"
msgstr "Kode"

#: searx/templates/oscar/stats.html:135 searx/templates/simple/stats.html:134
msgid "Checker"
msgstr "Sjekker"

#: searx/templates/oscar/stats.html:138 searx/templates/simple/stats.html:137
msgid "Failed test"
msgstr "Mislykket test"

#: searx/templates/oscar/stats.html:139 searx/templates/simple/stats.html:138
msgid "Comment(s)"
msgstr "Kommentar(er)"

#: searx/templates/oscar/time-range.html:5
#: searx/templates/simple/time-range.html:3
msgid "Anytime"
msgstr "Når som helst"

#: searx/templates/oscar/time-range.html:8
#: searx/templates/simple/time-range.html:6
msgid "Last day"
msgstr "Siste dag"

#: searx/templates/oscar/time-range.html:11
#: searx/templates/simple/time-range.html:9
msgid "Last week"
msgstr "Siste uke"

#: searx/templates/oscar/time-range.html:14
#: searx/templates/simple/time-range.html:12
msgid "Last month"
msgstr "Siste måned"

#: searx/templates/oscar/time-range.html:17
#: searx/templates/simple/time-range.html:15
msgid "Last year"
msgstr "Siste år"

#: searx/templates/oscar/messages/first_time.html:6
#: searx/templates/oscar/messages/no_data_available.html:3
msgid "Heads up!"
msgstr "Obs!"

#: searx/templates/oscar/messages/first_time.html:7
msgid "It look like you are using SearXNG first time."
msgstr ""

#: searx/templates/oscar/messages/no_cookies.html:3
msgid "Information!"
msgstr "Info."

#: searx/templates/oscar/messages/no_cookies.html:4
msgid "currently, there are no cookies defined."
msgstr "det er ingen informasjonskapsler definert per nå."

#: searx/templates/oscar/messages/no_data_available.html:4
#: searx/templates/simple/stats.html:24
msgid "There is currently no data available. "
msgstr "Ingen data tilgjengelig for øyeblikket. "

#: searx/templates/oscar/messages/no_results.html:4
#: searx/templates/simple/messages/no_results.html:4
msgid "Engines cannot retrieve results."
msgstr "Søkemotorene kan ikke hente inn resultater."

#: searx/templates/oscar/messages/no_results.html:13
#: searx/templates/simple/messages/no_results.html:14
msgid "Please, try again later or find another SearXNG instance."
msgstr ""

#: searx/templates/oscar/messages/no_results.html:17
#: searx/templates/simple/messages/no_results.html:18
msgid "Sorry!"
msgstr "Beklager."

#: searx/templates/oscar/messages/no_results.html:18
#: searx/templates/simple/messages/no_results.html:19
msgid ""
"we didn't find any results. Please use another query or search in more "
"categories."
msgstr "fant ingen resultater. Søk etter noe annet, eller i flere kategorier."

#: searx/templates/oscar/messages/save_settings_successfull.html:7
msgid "Well done!"
msgstr "Bra gjort."

#: searx/templates/oscar/messages/save_settings_successfull.html:8
msgid "Settings saved successfully."
msgstr "Innstillinger lagret."

#: searx/templates/oscar/messages/unknow_error.html:7
msgid "Oh snap!"
msgstr "Oida."

#: searx/templates/oscar/messages/unknow_error.html:8
msgid "Something went wrong."
msgstr "Noe gikk galt."

#: searx/templates/oscar/result_templates/default.html:7
#: searx/templates/oscar/result_templates/files.html:7
#: searx/templates/oscar/result_templates/files.html:10
#: searx/templates/simple/result_templates/default.html:6
msgid "show media"
msgstr "vis media"

#: searx/templates/oscar/result_templates/default.html:7
#: searx/templates/oscar/result_templates/files.html:7
#: searx/templates/simple/result_templates/default.html:6
msgid "hide media"
msgstr "skjul media"

#: searx/templates/oscar/result_templates/files.html:33
#: searx/templates/oscar/result_templates/videos.html:19
#: searx/templates/simple/result_templates/images.html:21
msgid "Author"
msgstr "Opphavsmann"

#: searx/templates/oscar/result_templates/files.html:37
#: searx/templates/oscar/result_templates/torrent.html:7
#: searx/templates/simple/result_templates/torrent.html:11
msgid "Filesize"
msgstr "Filstørrelse"

#: searx/templates/oscar/result_templates/files.html:38
#: searx/templates/oscar/result_templates/torrent.html:9
#: searx/templates/simple/result_templates/torrent.html:12
msgid "Bytes"
msgstr "Byte"

#: searx/templates/oscar/result_templates/files.html:39
#: searx/templates/oscar/result_templates/torrent.html:10
#: searx/templates/simple/result_templates/torrent.html:13
msgid "kiB"
msgstr "kiB"

#: searx/templates/oscar/result_templates/files.html:40
#: searx/templates/oscar/result_templates/torrent.html:11
#: searx/templates/simple/result_templates/torrent.html:14
msgid "MiB"
msgstr "MiB"

#: searx/templates/oscar/result_templates/files.html:41
#: searx/templates/oscar/result_templates/torrent.html:12
#: searx/templates/simple/result_templates/torrent.html:15
msgid "GiB"
msgstr "GiB"

#: searx/templates/oscar/result_templates/files.html:42
#: searx/templates/oscar/result_templates/torrent.html:13
#: searx/templates/simple/result_templates/torrent.html:16
msgid "TiB"
msgstr "TiB"

#: searx/templates/oscar/result_templates/files.html:46
msgid "Date"
msgstr "Dato"

#: searx/templates/oscar/result_templates/files.html:48
msgid "Type"
msgstr "Type"

#: searx/templates/oscar/result_templates/images.html:27
msgid "Get image"
msgstr "Hent bilde"

#: searx/templates/oscar/result_templates/images.html:30
#: searx/templates/simple/result_templates/images.html:25
msgid "View source"
msgstr "Vis kilde"

#: searx/templates/oscar/result_templates/map.html:26
#: searx/templates/simple/result_templates/map.html:11
msgid "address"
msgstr "adresse"

#: searx/templates/oscar/result_templates/map.html:59
#: searx/templates/simple/result_templates/map.html:42
msgid "show map"
msgstr "vis kart"

#: searx/templates/oscar/result_templates/map.html:59
#: searx/templates/simple/result_templates/map.html:42
msgid "hide map"
msgstr "skjul kart"

#: searx/templates/oscar/result_templates/torrent.html:6
#: searx/templates/simple/result_templates/torrent.html:9
msgid "Seeder"
msgstr "Deler"

#: searx/templates/oscar/result_templates/torrent.html:6
#: searx/templates/simple/result_templates/torrent.html:9
msgid "Leecher"
msgstr "Henter"

#: searx/templates/oscar/result_templates/torrent.html:15
#: searx/templates/simple/result_templates/torrent.html:20
msgid "Number of Files"
msgstr "Antall filer"

#: searx/templates/oscar/result_templates/videos.html:7
#: searx/templates/simple/result_templates/videos.html:6
msgid "show video"
msgstr "vis video"

#: searx/templates/oscar/result_templates/videos.html:7
#: searx/templates/simple/result_templates/videos.html:6
msgid "hide video"
msgstr "skjul video"

#: searx/templates/oscar/result_templates/videos.html:20
msgid "Length"
msgstr "Lengde"

#: searx/templates/simple/categories.html:6
msgid "Click on the magnifier to perform search"
msgstr "Klikk på forstørrelsesglasset for å søke"

#: searx/templates/simple/preferences.html:84
msgid "Errors:"
msgstr "Feil:"

#: searx/templates/simple/preferences.html:173
msgid "User interface"
msgstr "Brukergrensesnitt"

#: searx/templates/simple/preferences.html:244
msgid "Currently used search engines"
msgstr "Brukte søkemotorer"

#: searx/templates/simple/preferences.html:254
msgid "Supports selected language"
msgstr "Støtter valgt språk"

#: searx/templates/simple/results.html:24
msgid "Answers"
msgstr "Svar"

#: searx/templates/simple/result_templates/images.html:22
msgid "Format"
msgstr ""

#: searx/templates/simple/result_templates/images.html:24
msgid "Engine"
msgstr ""

#~ msgid "Change searx layout"
#~ msgstr "Endre searx-oppsett"

#~ msgid "Proxying image results through searx"
#~ msgstr "Mellomtjener bilderesultater gjennom searx"

#~ msgid "This is the list of searx's instant answering modules."
#~ msgstr "Dette er en liste over moduler for umiddelbare svar i searx."

#~ msgid ""
#~ "This is the list of cookies and"
#~ " their values searx is storing on "
#~ "your computer."
#~ msgstr ""
#~ "Dette er en liste over kaker og"
#~ " verdiene i dem som searx lagrer "
#~ "på datamaskinen din."

#~ msgid "With that list, you can assess searx transparency."
#~ msgstr "Med denne listen kan du bedømme searx-åpenhet."

#~ msgid "It look like you are using searx first time."
#~ msgstr "Det ser ut til at du bruker searx for første gang."

#~ msgid "Please, try again later or find another searx instance."
#~ msgstr "Prøv senere eller finn en annen searx-instans."

#~ msgid "Themes"
#~ msgstr "Drakter"

#~ msgid "Reliablity"
#~ msgstr "Pålitelighet"
