# Danish translations for .
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the  project.
#
# Translators:
# Mikkel Kirkgaard Nielsen <memb_transifex@mikini.dk>, 2018
# Morten Krogh Andersen <spam1@krogh.net>, 2017
msgid ""
msgstr ""
"Project-Id-Version:  searx\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2021-10-28 06:54+0000\n"
"PO-Revision-Date: 2020-07-09 13:10+0000\n"
"Last-Translator: Mikkel Kirkgaard Nielsen <memb_transifex@mikini.dk>\n"
"Language: da\n"
"Language-Team: Danish "
"(http://www.transifex.com/asciimoo/searx/language/da/)\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.9.1\n"

#: searx/webapp.py:164
msgid "files"
msgstr "filer"

#: searx/webapp.py:165
msgid "general"
msgstr "generelt"

#: searx/webapp.py:166
msgid "music"
msgstr "musik"

#: searx/webapp.py:167
msgid "social media"
msgstr "sociale medier"

#: searx/webapp.py:168
msgid "images"
msgstr "billeder"

#: searx/webapp.py:169
msgid "videos"
msgstr "videoer"

#: searx/webapp.py:170
msgid "it"
msgstr "it"

#: searx/webapp.py:171
msgid "news"
msgstr "nyheder"

#: searx/webapp.py:172
msgid "map"
msgstr "kort"

#: searx/webapp.py:173
msgid "onions"
msgstr ""

#: searx/webapp.py:174
msgid "science"
msgstr "videnskab"

#: searx/webapp.py:178
msgid "timeout"
msgstr ""

#: searx/webapp.py:179
msgid "parsing error"
msgstr ""

#: searx/webapp.py:180
msgid "HTTP protocol error"
msgstr ""

#: searx/webapp.py:181
msgid "network error"
msgstr ""

#: searx/webapp.py:183
msgid "unexpected crash"
msgstr ""

#: searx/webapp.py:190
msgid "HTTP error"
msgstr ""

#: searx/webapp.py:191
msgid "HTTP connection error"
msgstr ""

#: searx/webapp.py:197
msgid "proxy error"
msgstr ""

#: searx/webapp.py:198
msgid "CAPTCHA"
msgstr ""

#: searx/webapp.py:199
msgid "too many requests"
msgstr ""

#: searx/webapp.py:200
msgid "access denied"
msgstr ""

#: searx/webapp.py:201
msgid "server API error"
msgstr ""

#: searx/webapp.py:393
msgid "No item found"
msgstr "Intet fundet"

#: searx/engines/qwant.py:198
#: searx/templates/simple/result_templates/images.html:23 searx/webapp.py:395
msgid "Source"
msgstr ""

#: searx/webapp.py:505 searx/webapp.py:912
msgid "Invalid settings, please edit your preferences"
msgstr "Ugyldige indstillinger, redigér venligst dine valg"

#: searx/webapp.py:521
msgid "Invalid settings"
msgstr "Ugyldig indstilling"

#: searx/webapp.py:599 searx/webapp.py:665
msgid "search error"
msgstr "søgefejl"

#: searx/webapp.py:708
msgid "{minutes} minute(s) ago"
msgstr "for {minutes} minut(ter) siden"

#: searx/webapp.py:710
msgid "{hours} hour(s), {minutes} minute(s) ago"
msgstr "for {hours} time(r) og {minutes} minut(ter) siden"

#: searx/webapp.py:833
msgid "Suspended"
msgstr ""

#: searx/answerers/random/answerer.py:65
msgid "Random value generator"
msgstr "Generator af tilfældig værdi"

#: searx/answerers/random/answerer.py:66
msgid "Generate different random values"
msgstr "Generér forskellige tilfældige værdier"

#: searx/answerers/statistics/answerer.py:50
msgid "Statistics functions"
msgstr "Statistiske funktioner"

#: searx/answerers/statistics/answerer.py:51
msgid "Compute {functions} of the arguments"
msgstr "Beregn {functions} af parametrene"

#: searx/engines/openstreetmap.py:155
msgid "Get directions"
msgstr ""

#: searx/engines/pdbe.py:90
msgid "{title} (OBSOLETE)"
msgstr ""

#: searx/engines/pdbe.py:97
msgid "This entry has been superseded by"
msgstr "Denne værdi er blevet overskrevet af"

#: searx/engines/pubmed.py:78
msgid "No abstract is available for this publication."
msgstr "Intet sammendrag er tilgængelig for denne publikation."

#: searx/engines/qwant.py:200
msgid "Channel"
msgstr ""

#: searx/plugins/hash_plugin.py:24
msgid "Converts strings to different hash digests."
msgstr ""

#: searx/plugins/hash_plugin.py:52
msgid "hash digest"
msgstr ""

#: searx/plugins/hostname_replace.py:9
msgid "Hostname replace"
msgstr ""

#: searx/plugins/hostname_replace.py:10
msgid "Rewrite result hostnames or remove results based on the hostname"
msgstr ""

#: searx/plugins/infinite_scroll.py:3
msgid "Infinite scroll"
msgstr "Uendelig scrolling"

#: searx/plugins/infinite_scroll.py:4
msgid "Automatically load next page when scrolling to bottom of current page"
msgstr ""
"Indlæs automatisk næste side, når der scrolles til bunden af den "
"nuværende side"

#: searx/plugins/oa_doi_rewrite.py:9
msgid "Open Access DOI rewrite"
msgstr "Open Access DOI-omskrivning"

#: searx/plugins/oa_doi_rewrite.py:10
msgid ""
"Avoid paywalls by redirecting to open-access versions of publications "
"when available"
msgstr ""
"Undgå betalingsmure ved at viderestille til en åbent tilgængelig version,"
" hvis en sådan findes"

#: searx/plugins/search_on_category_select.py:18
msgid "Search on category select"
msgstr "Søg på kategori i stedet"

#: searx/plugins/search_on_category_select.py:19
msgid ""
"Perform search immediately if a category selected. Disable to select "
"multiple categories. (JavaScript required)"
msgstr ""
"Udfør søgning straks, hvis en kategori vælges. Slå dette fra for at kunne"
" vælge flere kategorier (JavaScript påkrævet)"

#: searx/plugins/self_info.py:19
msgid "Self Informations"
msgstr ""

#: searx/plugins/self_info.py:20
msgid ""
"Displays your IP if the query is \"ip\" and your user agent if the query "
"contains \"user agent\"."
msgstr ""
"Viser din IP adresse hvis søgningen er \"ip\" og din user-agent i "
"søgningen indeholder \"user agent\""

#: searx/plugins/tracker_url_remover.py:27
msgid "Tracker URL remover"
msgstr "Fjernelse af tracker URL"

#: searx/plugins/tracker_url_remover.py:28
msgid "Remove trackers arguments from the returned URL"
msgstr "Fjern trackeres parametre fra den returnerede URL"

#: searx/plugins/vim_hotkeys.py:3
msgid "Vim-like hotkeys"
msgstr "Genvejstaster i Vim-stil"

#: searx/plugins/vim_hotkeys.py:4
msgid ""
"Navigate search results with Vim-like hotkeys (JavaScript required). "
"Press \"h\" key on main or result page to get help."
msgstr ""
"Navigér søgeresultater med Vim-lignende genvejstaster (JavaScript "
"påkrævet). Tryk på \"h\" på hoved- eller resultatsiden for at få hjælp."

#: searx/templates/oscar/404.html:4 searx/templates/simple/404.html:4
msgid "Page not found"
msgstr "Side ikke fundet"

#: searx/templates/oscar/404.html:6 searx/templates/simple/404.html:6
#, python-format
msgid "Go to %(search_page)s."
msgstr "Gå til 1%(search_page)s"

#: searx/templates/oscar/404.html:6 searx/templates/simple/404.html:6
msgid "search page"
msgstr "søgeside"

#: searx/templates/oscar/about.html:2 searx/templates/oscar/navbar.html:6
msgid "about"
msgstr "om"

#: searx/templates/oscar/advanced.html:4
msgid "Advanced settings"
msgstr "Avancerede indstillinger"

#: searx/templates/oscar/base.html:55
#: searx/templates/oscar/messages/first_time.html:4
#: searx/templates/oscar/messages/save_settings_successfull.html:5
#: searx/templates/oscar/messages/unknow_error.html:5
msgid "Close"
msgstr "Luk"

#: searx/templates/oscar/base.html:57
#: searx/templates/oscar/messages/no_results.html:4
#: searx/templates/simple/messages/no_results.html:4
#: searx/templates/simple/results.html:45
msgid "Error!"
msgstr "Fejl!"

#: searx/templates/oscar/base.html:85 searx/templates/simple/base.html:53
msgid "Powered by"
msgstr "Leveret af"

#: searx/templates/oscar/base.html:85 searx/templates/simple/base.html:53
msgid "a privacy-respecting, hackable metasearch engine"
msgstr "en privatlivs--respekterende, hackbar meta-søgemaskine"

#: searx/templates/oscar/base.html:86 searx/templates/simple/base.html:54
msgid "Source code"
msgstr ""

#: searx/templates/oscar/base.html:87 searx/templates/simple/base.html:55
msgid "Issue tracker"
msgstr ""

#: searx/templates/oscar/base.html:88 searx/templates/oscar/stats.html:18
#: searx/templates/simple/base.html:56 searx/templates/simple/stats.html:21
msgid "Engine stats"
msgstr "Søgemaskine-statistik"

#: searx/templates/oscar/base.html:89
#: searx/templates/oscar/messages/no_results.html:13
#: searx/templates/simple/base.html:57
#: searx/templates/simple/messages/no_results.html:14
msgid "Public instances"
msgstr ""

#: searx/templates/oscar/base.html:90 searx/templates/simple/base.html:58
msgid "Contact instance maintainer"
msgstr ""

#: searx/templates/oscar/languages.html:2
msgid "Language"
msgstr ""

#: searx/templates/oscar/languages.html:4
#: searx/templates/simple/languages.html:2
#: searx/templates/simple/preferences.html:119
msgid "Default language"
msgstr "Standardsprog"

#: searx/templates/oscar/macros.html:23
#: searx/templates/simple/result_templates/torrent.html:6
msgid "magnet link"
msgstr "magnet-link"

#: searx/templates/oscar/macros.html:24
#: searx/templates/simple/result_templates/torrent.html:7
msgid "torrent file"
msgstr "torrent-fil"

#: searx/templates/oscar/macros.html:37 searx/templates/oscar/macros.html:39
#: searx/templates/oscar/macros.html:73 searx/templates/oscar/macros.html:75
#: searx/templates/simple/macros.html:46
msgid "cached"
msgstr "cached"

#: searx/templates/oscar/macros.html:43 searx/templates/oscar/macros.html:59
#: searx/templates/oscar/macros.html:79 searx/templates/oscar/macros.html:93
#: searx/templates/simple/macros.html:46
msgid "proxied"
msgstr "viderestillet"

#: searx/templates/oscar/macros.html:133
#: searx/templates/oscar/preferences.html:319
#: searx/templates/oscar/preferences.html:337
#: searx/templates/oscar/preferences.html:391
#: searx/templates/simple/preferences.html:251
#: searx/templates/simple/preferences.html:289
msgid "Allow"
msgstr "Tillad"

#: searx/templates/oscar/macros.html:139
msgid "broken"
msgstr ""

#: searx/templates/oscar/macros.html:141
msgid "supported"
msgstr "understøttet"

#: searx/templates/oscar/macros.html:143
msgid "not supported"
msgstr "ikke-understøttet"

#: searx/templates/oscar/navbar.html:7
#: searx/templates/oscar/preferences.html:90
msgid "preferences"
msgstr "indstillinger"

#: searx/templates/oscar/preferences.html:12
#: searx/templates/simple/preferences.html:28
msgid "No HTTPS"
msgstr ""

#: searx/templates/oscar/preferences.html:14
#: searx/templates/oscar/results.html:27 searx/templates/simple/results.html:40
msgid "Number of results"
msgstr "Antal resultater"

#: searx/templates/oscar/preferences.html:14
msgid "Avg."
msgstr ""

#: searx/templates/oscar/messages/no_results.html:8
#: searx/templates/oscar/preferences.html:17
#: searx/templates/oscar/preferences.html:18
#: searx/templates/oscar/results.html:36
#: searx/templates/simple/messages/no_results.html:8
#: searx/templates/simple/preferences.html:30
#: searx/templates/simple/preferences.html:31
#: searx/templates/simple/results.html:48
msgid "View error logs and submit a bug report"
msgstr ""

#: searx/templates/oscar/preferences.html:38
#: searx/templates/oscar/stats.html:70
#: searx/templates/simple/preferences.html:52
#: searx/templates/simple/stats.html:70
msgid "Median"
msgstr ""

#: searx/templates/oscar/preferences.html:39
#: searx/templates/oscar/stats.html:76
#: searx/templates/simple/preferences.html:53
#: searx/templates/simple/stats.html:76
msgid "P80"
msgstr ""

#: searx/templates/oscar/preferences.html:40
#: searx/templates/oscar/stats.html:82
#: searx/templates/simple/preferences.html:54
#: searx/templates/simple/stats.html:82
msgid "P95"
msgstr ""

#: searx/templates/oscar/preferences.html:68
#: searx/templates/simple/preferences.html:82
msgid "Failed checker test(s): "
msgstr ""

#: searx/templates/oscar/preferences.html:96
#: searx/templates/simple/preferences.html:100
msgid "Preferences"
msgstr "Indstillinger"

#: searx/templates/oscar/preferences.html:101
#: searx/templates/oscar/preferences.html:111
#: searx/templates/simple/preferences.html:106
msgid "General"
msgstr "Generelt"

#: searx/templates/oscar/preferences.html:102
#: searx/templates/oscar/preferences.html:193
msgid "User Interface"
msgstr ""

#: searx/templates/oscar/preferences.html:103
#: searx/templates/oscar/preferences.html:257
#: searx/templates/simple/preferences.html:215
msgid "Privacy"
msgstr "Privatliv"

#: searx/templates/oscar/preferences.html:104
#: searx/templates/oscar/preferences.html:296
#: searx/templates/simple/preferences.html:243
msgid "Engines"
msgstr "Søgemaskiner"

#: searx/templates/oscar/preferences.html:105
#: searx/templates/simple/preferences.html:284
msgid "Special Queries"
msgstr ""

#: searx/templates/oscar/preferences.html:106
#: searx/templates/oscar/preferences.html:431
#: searx/templates/simple/preferences.html:324
msgid "Cookies"
msgstr "Cookies"

#: searx/templates/oscar/preferences.html:123
#: searx/templates/oscar/preferences.html:125
#: searx/templates/simple/preferences.html:109
msgid "Default categories"
msgstr "Standardkategorier"

#: searx/templates/oscar/preferences.html:133
#: searx/templates/simple/preferences.html:116
#: searx/templates/simple/preferences.html:225
msgid "Search language"
msgstr "Søgesprog"

#: searx/templates/oscar/preferences.html:134
#: searx/templates/simple/preferences.html:125
msgid "What language do you prefer for search?"
msgstr "Hvilket sprog foretrækker du til søgninger?"

#: searx/templates/oscar/preferences.html:141
#: searx/templates/oscar/preferences.html:323
#: searx/templates/oscar/preferences.html:333
#: searx/templates/simple/preferences.html:144
#: searx/templates/simple/preferences.html:255
msgid "SafeSearch"
msgstr "SafeSearch"

#: searx/templates/oscar/preferences.html:142
#: searx/templates/simple/preferences.html:152
msgid "Filter content"
msgstr "Filtrér indhold"

#: searx/templates/oscar/preferences.html:145
#: searx/templates/simple/preferences.html:147
msgid "Strict"
msgstr "Stringent"

#: searx/templates/oscar/preferences.html:146
#: searx/templates/simple/preferences.html:148
msgid "Moderate"
msgstr "Moderat"

#: searx/templates/oscar/preferences.html:147
#: searx/templates/simple/preferences.html:149
msgid "None"
msgstr "Ingen"

#: searx/templates/oscar/preferences.html:153
#: searx/templates/simple/preferences.html:130
msgid "Autocomplete"
msgstr "Automatisk fuldførelse"

#: searx/templates/oscar/preferences.html:154
#: searx/templates/simple/preferences.html:139
msgid "Find stuff as you type"
msgstr "Find under indtastning"

#: searx/templates/oscar/preferences.html:168
#: searx/templates/simple/preferences.html:158
msgid "Open Access DOI resolver"
msgstr "Open Access DOI-forløser"

#: searx/templates/oscar/preferences.html:169
#: searx/templates/simple/preferences.html:168
msgid ""
"Redirect to open-access versions of publications when available (plugin "
"required)"
msgstr ""
"Omdiriger til open-access-udgaver af publikationer hvis tilgængelig "
"(plugin påkrævet)"

#: searx/templates/oscar/preferences.html:183
msgid "Engine tokens"
msgstr ""

#: searx/templates/oscar/preferences.html:184
msgid "Access tokens for private engines"
msgstr ""

#: searx/templates/oscar/preferences.html:198
#: searx/templates/simple/preferences.html:176
msgid "Interface language"
msgstr "Sprog i brugergrænsefladen"

#: searx/templates/oscar/preferences.html:199
#: searx/templates/simple/preferences.html:184
msgid "Change the language of the layout"
msgstr "Ændring af layout-sproget"

#: searx/templates/oscar/preferences.html:210
#: searx/templates/simple/preferences.html:189
msgid "Theme"
msgstr ""

#: searx/templates/oscar/preferences.html:211
#: searx/templates/simple/preferences.html:197
msgid "Change SearXNG layout"
msgstr ""

#: searx/templates/oscar/preferences.html:222
#: searx/templates/oscar/preferences.html:228
msgid "Choose style for this theme"
msgstr "Vælg stil for dette tema"

#: searx/templates/oscar/preferences.html:222
#: searx/templates/oscar/preferences.html:228
msgid "Style"
msgstr "Stil"

#: searx/templates/oscar/preferences.html:231
msgid "Show advanced settings"
msgstr ""

#: searx/templates/oscar/preferences.html:232
msgid "Show advanced settings panel in the home page by default"
msgstr ""

#: searx/templates/oscar/preferences.html:235
#: searx/templates/oscar/preferences.html:245
#: searx/templates/simple/preferences.html:205
msgid "On"
msgstr "Til"

#: searx/templates/oscar/preferences.html:236
#: searx/templates/oscar/preferences.html:246
#: searx/templates/simple/preferences.html:206
msgid "Off"
msgstr "Fra"

#: searx/templates/oscar/preferences.html:241
#: searx/templates/simple/preferences.html:202
msgid "Results on new tabs"
msgstr "Resultater på nye tabs"

#: searx/templates/oscar/preferences.html:242
#: searx/templates/simple/preferences.html:209
msgid "Open result links on new browser tabs"
msgstr "Åben resultat-link i en ny browser-tab"

#: searx/templates/oscar/preferences.html:262
#: searx/templates/simple/preferences.html:218
msgid "Method"
msgstr "Metode"

#: searx/templates/oscar/preferences.html:263
msgid ""
"Change how forms are submited, <a "
"href=\"http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods\""
" rel=\"external\">learn more about request methods</a>"
msgstr ""
"Ændring af hvordan webforms indsendes, <a "
"href=\"http://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods\""
" rel=\"external\">lær mere om request-metoder</a>"

#: searx/templates/oscar/preferences.html:273
#: searx/templates/simple/preferences.html:230
msgid "Image proxy"
msgstr "Billede-proxy"

#: searx/templates/oscar/preferences.html:274
#: searx/templates/simple/preferences.html:237
msgid "Proxying image results through SearXNG"
msgstr ""

#: searx/templates/oscar/preferences.html:277
#: searx/templates/simple/preferences.html:233
msgid "Enabled"
msgstr "Slået til"

#: searx/templates/oscar/preferences.html:278
#: searx/templates/simple/preferences.html:234
msgid "Disabled"
msgstr "Slået fra"

#: searx/templates/oscar/preferences.html:304
msgid "Allow all"
msgstr ""

#: searx/templates/oscar/preferences.html:305
msgid "Disable all"
msgstr ""

#: searx/templates/oscar/preferences.html:320
#: searx/templates/oscar/preferences.html:336
#: searx/templates/oscar/stats.html:29
#: searx/templates/simple/preferences.html:252
#: searx/templates/simple/stats.html:28
msgid "Engine name"
msgstr "Søgemaskinenavn"

#: searx/templates/oscar/preferences.html:321
#: searx/templates/oscar/preferences.html:335
#: searx/templates/simple/preferences.html:253
msgid "Shortcut"
msgstr "Genvej"

#: searx/templates/oscar/preferences.html:322
#: searx/templates/oscar/preferences.html:334
msgid "Selected language"
msgstr "Valgt sprog"

#: searx/templates/oscar/preferences.html:324
#: searx/templates/oscar/preferences.html:332
#: searx/templates/oscar/time-range.html:2
#: searx/templates/simple/preferences.html:256
msgid "Time range"
msgstr "Tidsinterval"

#: searx/templates/oscar/preferences.html:325
#: searx/templates/oscar/preferences.html:331
#: searx/templates/oscar/stats.html:32
#: searx/templates/simple/preferences.html:257
#: searx/templates/simple/stats.html:31
msgid "Response time"
msgstr ""

#: searx/templates/oscar/preferences.html:326
#: searx/templates/oscar/preferences.html:330
#: searx/templates/simple/preferences.html:258
msgid "Max time"
msgstr "Maks-tid"

#: searx/templates/oscar/preferences.html:327
#: searx/templates/oscar/preferences.html:329
#: searx/templates/oscar/stats.html:33
#: searx/templates/simple/preferences.html:259
#: searx/templates/simple/stats.html:32
msgid "Reliability"
msgstr ""

#: searx/templates/oscar/preferences.html:385
msgid "Query"
msgstr ""

#: searx/templates/oscar/preferences.html:392
#: searx/templates/simple/preferences.html:290
msgid "Keywords"
msgstr "Nøgleord"

#: searx/templates/oscar/preferences.html:393
#: searx/templates/simple/preferences.html:291
msgid "Name"
msgstr "Navn"

#: searx/templates/oscar/preferences.html:394
#: searx/templates/simple/preferences.html:292
msgid "Description"
msgstr "Beskrivelse"

#: searx/templates/oscar/preferences.html:395
#: searx/templates/simple/preferences.html:293
msgid "Examples"
msgstr "Eksempler"

#: searx/templates/oscar/preferences.html:400
#: searx/templates/simple/preferences.html:296
msgid "This is the list of SearXNG's instant answering modules."
msgstr ""

#: searx/templates/oscar/preferences.html:413
#: searx/templates/simple/preferences.html:307
msgid "This is the list of plugins."
msgstr ""

#: searx/templates/oscar/preferences.html:434
#: searx/templates/simple/preferences.html:326
msgid ""
"This is the list of cookies and their values SearXNG is storing on your "
"computer."
msgstr ""

#: searx/templates/oscar/preferences.html:435
#: searx/templates/simple/preferences.html:327
msgid "With that list, you can assess SearXNG transparency."
msgstr ""

#: searx/templates/oscar/preferences.html:440
#: searx/templates/simple/preferences.html:332
msgid "Cookie name"
msgstr "Cookie-navn"

#: searx/templates/oscar/preferences.html:441
#: searx/templates/simple/preferences.html:333
msgid "Value"
msgstr "Værdi"

#: searx/templates/oscar/preferences.html:458
#: searx/templates/simple/preferences.html:354
msgid ""
"These settings are stored in your cookies, this allows us not to store "
"this data about you."
msgstr ""
"Disse indstillnger gemmes cookies på din enhed. Dette gør, at vi ikke "
"behøver at gemme data om dig"

#: searx/templates/oscar/preferences.html:459
#: searx/templates/simple/preferences.html:356
msgid ""
"These cookies serve your sole convenience, we don't use these cookies to "
"track you."
msgstr ""
"Disse cookies er kun til dine data. Vi benytter ikke disse til at spore "
"dig."

#: searx/templates/oscar/preferences.html:463
#: searx/templates/simple/preferences.html:345
msgid "Search URL of the currently saved preferences"
msgstr "Søge-URL for den nuværende gemte indstilling"

#: searx/templates/oscar/preferences.html:464
#: searx/templates/simple/preferences.html:349
msgid ""
"Note: specifying custom settings in the search URL can reduce privacy by "
"leaking data to the clicked result sites."
msgstr ""
"Bemærk: brugertilpassede indstillinger i søge-URL kan reducere niveauet "
"af beskyttelse ved at lække data til de sider der klikkes på i "
"resultatet."

#: searx/templates/oscar/preferences.html:469
#: searx/templates/simple/preferences.html:359
msgid "save"
msgstr "gem"

#: searx/templates/oscar/preferences.html:470
#: searx/templates/simple/preferences.html:361
msgid "back"
msgstr "tilbage"

#: searx/templates/oscar/preferences.html:471
#: searx/templates/simple/preferences.html:360
msgid "Reset defaults"
msgstr "Nustil til standard"

#: searx/templates/oscar/results.html:32 searx/templates/simple/results.html:45
msgid "Engines cannot retrieve results"
msgstr "Søgemotorer kan ikke hente resultater"

#: searx/templates/oscar/results.html:53 searx/templates/simple/results.html:66
msgid "Suggestions"
msgstr "Forslag"

#: searx/templates/oscar/results.html:74
msgid "Links"
msgstr "Links"

#: searx/templates/oscar/results.html:79 searx/templates/simple/results.html:84
msgid "Search URL"
msgstr "Søge-URL"

#: searx/templates/oscar/results.html:84 searx/templates/simple/results.html:89
msgid "Download results"
msgstr "Hent resultater"

#: searx/templates/oscar/results.html:95
msgid "RSS subscription"
msgstr ""

#: searx/templates/oscar/results.html:104
msgid "Search results"
msgstr "Søgereresultater"

#: searx/templates/oscar/results.html:109
#: searx/templates/simple/results.html:113
msgid "Try searching for:"
msgstr "Prøv at søge efter:"

#: searx/templates/oscar/results.html:162
#: searx/templates/oscar/results.html:187
#: searx/templates/simple/results.html:179
msgid "next page"
msgstr "næste side"

#: searx/templates/oscar/results.html:169
#: searx/templates/oscar/results.html:180
#: searx/templates/simple/results.html:162
msgid "previous page"
msgstr "forrige side"

#: searx/templates/oscar/search.html:6 searx/templates/oscar/search_full.html:9
#: searx/templates/simple/search.html:7
#: searx/templates/simple/simple_search.html:4
msgid "Search for..."
msgstr "Søg efter..."

#: searx/templates/oscar/search.html:8
#: searx/templates/oscar/search_full.html:11
#: searx/templates/simple/search.html:9
#: searx/templates/simple/simple_search.html:6
msgid "Start search"
msgstr "Start søgning"

#: searx/templates/oscar/search.html:9
#: searx/templates/oscar/search_full.html:12
#: searx/templates/simple/search.html:8
#: searx/templates/simple/simple_search.html:5
msgid "Clear search"
msgstr ""

#: searx/templates/oscar/search_full.html:12
msgid "Clear"
msgstr ""

#: searx/templates/oscar/stats.html:4
msgid "stats"
msgstr "statistik"

#: searx/templates/oscar/stats.html:30 searx/templates/simple/stats.html:29
msgid "Scores"
msgstr "Vægtninger"

#: searx/templates/oscar/stats.html:31 searx/templates/simple/stats.html:30
msgid "Result count"
msgstr ""

#: searx/templates/oscar/stats.html:42 searx/templates/simple/stats.html:41
msgid "Scores per result"
msgstr "Vægtninger pr. resultat"

#: searx/templates/oscar/stats.html:65 searx/templates/simple/stats.html:65
msgid "Total"
msgstr ""

#: searx/templates/oscar/stats.html:66 searx/templates/simple/stats.html:66
msgid "HTTP"
msgstr ""

#: searx/templates/oscar/stats.html:67 searx/templates/simple/stats.html:67
msgid "Processing"
msgstr ""

#: searx/templates/oscar/stats.html:106 searx/templates/simple/stats.html:105
msgid "Warnings"
msgstr ""

#: searx/templates/oscar/stats.html:106 searx/templates/simple/stats.html:105
msgid "Errors and exceptions"
msgstr ""

#: searx/templates/oscar/stats.html:112 searx/templates/simple/stats.html:111
msgid "Exception"
msgstr ""

#: searx/templates/oscar/stats.html:114 searx/templates/simple/stats.html:113
msgid "Message"
msgstr ""

#: searx/templates/oscar/stats.html:116 searx/templates/simple/stats.html:115
msgid "Percentage"
msgstr ""

#: searx/templates/oscar/stats.html:118 searx/templates/simple/stats.html:117
msgid "Parameter"
msgstr ""

#: searx/templates/oscar/result_templates/files.html:35
#: searx/templates/oscar/stats.html:126 searx/templates/simple/stats.html:125
msgid "Filename"
msgstr ""

#: searx/templates/oscar/stats.html:127 searx/templates/simple/stats.html:126
msgid "Function"
msgstr ""

#: searx/templates/oscar/stats.html:128 searx/templates/simple/stats.html:127
msgid "Code"
msgstr ""

#: searx/templates/oscar/stats.html:135 searx/templates/simple/stats.html:134
msgid "Checker"
msgstr ""

#: searx/templates/oscar/stats.html:138 searx/templates/simple/stats.html:137
msgid "Failed test"
msgstr ""

#: searx/templates/oscar/stats.html:139 searx/templates/simple/stats.html:138
msgid "Comment(s)"
msgstr ""

#: searx/templates/oscar/time-range.html:5
#: searx/templates/simple/time-range.html:3
msgid "Anytime"
msgstr "Når som helst"

#: searx/templates/oscar/time-range.html:8
#: searx/templates/simple/time-range.html:6
msgid "Last day"
msgstr "Det seneste døgn"

#: searx/templates/oscar/time-range.html:11
#: searx/templates/simple/time-range.html:9
msgid "Last week"
msgstr "Den seneste uge"

#: searx/templates/oscar/time-range.html:14
#: searx/templates/simple/time-range.html:12
msgid "Last month"
msgstr "Den seneste måned"

#: searx/templates/oscar/time-range.html:17
#: searx/templates/simple/time-range.html:15
msgid "Last year"
msgstr "Det sidste år"

#: searx/templates/oscar/messages/first_time.html:6
#: searx/templates/oscar/messages/no_data_available.html:3
msgid "Heads up!"
msgstr "OBS!"

#: searx/templates/oscar/messages/first_time.html:7
msgid "It look like you are using SearXNG first time."
msgstr ""

#: searx/templates/oscar/messages/no_cookies.html:3
msgid "Information!"
msgstr "Information!"

#: searx/templates/oscar/messages/no_cookies.html:4
msgid "currently, there are no cookies defined."
msgstr "der er pt. ingen cookies defineret"

#: searx/templates/oscar/messages/no_data_available.html:4
#: searx/templates/simple/stats.html:24
msgid "There is currently no data available. "
msgstr "Der er pt. ingen tilgængelige data"

#: searx/templates/oscar/messages/no_results.html:4
#: searx/templates/simple/messages/no_results.html:4
msgid "Engines cannot retrieve results."
msgstr "Søgemotorer kan ikke hente resultater."

#: searx/templates/oscar/messages/no_results.html:13
#: searx/templates/simple/messages/no_results.html:14
msgid "Please, try again later or find another SearXNG instance."
msgstr ""

#: searx/templates/oscar/messages/no_results.html:17
#: searx/templates/simple/messages/no_results.html:18
msgid "Sorry!"
msgstr "Beklager!"

#: searx/templates/oscar/messages/no_results.html:18
#: searx/templates/simple/messages/no_results.html:19
msgid ""
"we didn't find any results. Please use another query or search in more "
"categories."
msgstr ""
"vi fandt ingen resultater. Benyt venligst en anden søge-streng eller søg "
"i flere kategorier"

#: searx/templates/oscar/messages/save_settings_successfull.html:7
msgid "Well done!"
msgstr "Godt klaret!"

#: searx/templates/oscar/messages/save_settings_successfull.html:8
msgid "Settings saved successfully."
msgstr "Indstillinger gemt."

#: searx/templates/oscar/messages/unknow_error.html:7
msgid "Oh snap!"
msgstr "Åh, pokkers!"

#: searx/templates/oscar/messages/unknow_error.html:8
msgid "Something went wrong."
msgstr "Noget gik galt"

#: searx/templates/oscar/result_templates/default.html:7
#: searx/templates/oscar/result_templates/files.html:7
#: searx/templates/oscar/result_templates/files.html:10
#: searx/templates/simple/result_templates/default.html:6
msgid "show media"
msgstr "vis media"

#: searx/templates/oscar/result_templates/default.html:7
#: searx/templates/oscar/result_templates/files.html:7
#: searx/templates/simple/result_templates/default.html:6
msgid "hide media"
msgstr "skjul media"

#: searx/templates/oscar/result_templates/files.html:33
#: searx/templates/oscar/result_templates/videos.html:19
#: searx/templates/simple/result_templates/images.html:21
msgid "Author"
msgstr ""

#: searx/templates/oscar/result_templates/files.html:37
#: searx/templates/oscar/result_templates/torrent.html:7
#: searx/templates/simple/result_templates/torrent.html:11
msgid "Filesize"
msgstr "Filstørrelse"

#: searx/templates/oscar/result_templates/files.html:38
#: searx/templates/oscar/result_templates/torrent.html:9
#: searx/templates/simple/result_templates/torrent.html:12
msgid "Bytes"
msgstr "Bytes"

#: searx/templates/oscar/result_templates/files.html:39
#: searx/templates/oscar/result_templates/torrent.html:10
#: searx/templates/simple/result_templates/torrent.html:13
msgid "kiB"
msgstr "kiB"

#: searx/templates/oscar/result_templates/files.html:40
#: searx/templates/oscar/result_templates/torrent.html:11
#: searx/templates/simple/result_templates/torrent.html:14
msgid "MiB"
msgstr "MiB"

#: searx/templates/oscar/result_templates/files.html:41
#: searx/templates/oscar/result_templates/torrent.html:12
#: searx/templates/simple/result_templates/torrent.html:15
msgid "GiB"
msgstr "GiB"

#: searx/templates/oscar/result_templates/files.html:42
#: searx/templates/oscar/result_templates/torrent.html:13
#: searx/templates/simple/result_templates/torrent.html:16
msgid "TiB"
msgstr "TiB"

#: searx/templates/oscar/result_templates/files.html:46
msgid "Date"
msgstr ""

#: searx/templates/oscar/result_templates/files.html:48
msgid "Type"
msgstr ""

#: searx/templates/oscar/result_templates/images.html:27
msgid "Get image"
msgstr "Hent billede"

#: searx/templates/oscar/result_templates/images.html:30
#: searx/templates/simple/result_templates/images.html:25
msgid "View source"
msgstr "Vis kilde"

#: searx/templates/oscar/result_templates/map.html:26
#: searx/templates/simple/result_templates/map.html:11
msgid "address"
msgstr ""

#: searx/templates/oscar/result_templates/map.html:59
#: searx/templates/simple/result_templates/map.html:42
msgid "show map"
msgstr "vis kort"

#: searx/templates/oscar/result_templates/map.html:59
#: searx/templates/simple/result_templates/map.html:42
msgid "hide map"
msgstr "skjul kort"

#: searx/templates/oscar/result_templates/torrent.html:6
#: searx/templates/simple/result_templates/torrent.html:9
msgid "Seeder"
msgstr "Afsender"

#: searx/templates/oscar/result_templates/torrent.html:6
#: searx/templates/simple/result_templates/torrent.html:9
msgid "Leecher"
msgstr "Henter"

#: searx/templates/oscar/result_templates/torrent.html:15
#: searx/templates/simple/result_templates/torrent.html:20
msgid "Number of Files"
msgstr "Antal filer"

#: searx/templates/oscar/result_templates/videos.html:7
#: searx/templates/simple/result_templates/videos.html:6
msgid "show video"
msgstr "vis video"

#: searx/templates/oscar/result_templates/videos.html:7
#: searx/templates/simple/result_templates/videos.html:6
msgid "hide video"
msgstr "skjul video"

#: searx/templates/oscar/result_templates/videos.html:20
msgid "Length"
msgstr ""

#: searx/templates/simple/categories.html:6
msgid "Click on the magnifier to perform search"
msgstr "Klik på forstørrelsesglasset for at udføre søgning"

#: searx/templates/simple/preferences.html:84
msgid "Errors:"
msgstr ""

#: searx/templates/simple/preferences.html:173
msgid "User interface"
msgstr "Brugerinterface"

#: searx/templates/simple/preferences.html:244
msgid "Currently used search engines"
msgstr "Pt. anvendte søgemaskiner"

#: searx/templates/simple/preferences.html:254
msgid "Supports selected language"
msgstr "Undstøtter valgte sprog"

#: searx/templates/simple/results.html:24
msgid "Answers"
msgstr "Svar"

#: searx/templates/simple/result_templates/images.html:22
msgid "Format"
msgstr ""

#: searx/templates/simple/result_templates/images.html:24
msgid "Engine"
msgstr ""

#~ msgid "Engine time (sec)"
#~ msgstr "Søgemaskine-tid (sek)"

#~ msgid "Page loads (sec)"
#~ msgstr "Sideindlæsninger (sek)"

#~ msgid "Errors"
#~ msgstr "Fejl"

#~ msgid "CAPTCHA required"
#~ msgstr ""

#~ msgid "Rewrite HTTP links to HTTPS if possible"
#~ msgstr "Omskriv HTTP links til HTTPS hvis muligt"

#~ msgid ""
#~ "Results are opened in the same "
#~ "window by default. This plugin "
#~ "overwrites the default behaviour to open"
#~ " links on new tabs/windows. (JavaScript "
#~ "required)"
#~ msgstr ""
#~ "Resultater åbnes som standard i det "
#~ "samme vindue. Dette plugin overskriver "
#~ "dette, således at link åbnes i nye"
#~ " tabs eller vinduer. (JavaScript påkrævet)"

#~ msgid "Color"
#~ msgstr "Farve"

#~ msgid "Blue (default)"
#~ msgstr "Blå (standard)"

#~ msgid "Violet"
#~ msgstr "Violet"

#~ msgid "Green"
#~ msgstr "Grøn"

#~ msgid "Cyan"
#~ msgstr "Cyan"

#~ msgid "Orange"
#~ msgstr "Orange"

#~ msgid "Red"
#~ msgstr "Rød"

#~ msgid "Category"
#~ msgstr "Kategori"

#~ msgid "Block"
#~ msgstr "Blokér"

#~ msgid "original context"
#~ msgstr "oprindelig sammenhæng"

#~ msgid "Plugins"
#~ msgstr "Plugins"

#~ msgid "Answerers"
#~ msgstr "Svarere"

#~ msgid "Avg. time"
#~ msgstr "Gns. tid"

#~ msgid "show details"
#~ msgstr "vis detaljer"

#~ msgid "hide details"
#~ msgstr "skjul detaljer"

#~ msgid "Load more..."
#~ msgstr "Indlæs mere..."

#~ msgid "Loading..."
#~ msgstr ""

#~ msgid "Change searx layout"
#~ msgstr "Ændring af searx layout"

#~ msgid "Proxying image results through searx"
#~ msgstr "Send billeder via searx"

#~ msgid "This is the list of searx's instant answering modules."
#~ msgstr "Dette er listen over searx's installationens svar-moduler"

#~ msgid ""
#~ "This is the list of cookies and"
#~ " their values searx is storing on "
#~ "your computer."
#~ msgstr "Dette er listen over de cookies og værdier searx gemmer på din computer"

#~ msgid "With that list, you can assess searx transparency."
#~ msgstr "Med denne liste, kan du bekræfte gennemsigtigheden af searx"

#~ msgid "It look like you are using searx first time."
#~ msgstr "Det ser ud til at benytter searx for første gang."

#~ msgid "Please, try again later or find another searx instance."
#~ msgstr "Vær venlig at prøve igen senere, eller find en anden searx-instans."

#~ msgid "Themes"
#~ msgstr "Temaer"

#~ msgid "Reliablity"
#~ msgstr ""

