whoogle=secven/whoogle
bunkerized=secven/bunkerized

DC_CF=docker-compose.cloudflare.yml
DC_PROD=docker-compose.prod.yml
DC_CFSSL=docker-compose.cfssl.yml

install:
	 sudo apt -y install docker docker-compose nano git curl
	 sudo usermod -aG docker $(USER)
	 newgrp docker

cf:
	 docker-compose -f $(DC_CF) up -d

prod:
	 docker-compose -f $(DC_PROD) up -d

cfssl:
	 docker-compose -f $(DC_CFSSL) up -d

build:
	 docker build -t bunkerized-nginx -f src/bunkerized-nginx/Dockerfile .
	 docker build -t whoogle-search:1.0 -f src/whoogle/Dockerfile .

push:
	 docker tag whoogle-search:1.0 $(whoogle)
	 docker push $(whoogle)
	 docker tag bunkerized-nginx $(bunkerized)
	 docker push $(bunkerized)

prune:
	 docker system prune -a

stop:
	 docker-compose -f $(DC_CF) down
	 docker-compose -f $(DC_PROD) dwon

censys:
	 sudo iptables -A INPUT -s 198.108.66.0/23 -j DROP
	 sudo iptables -A INPUT -s 47.205.232.0/21 -j DROP
