<!-- PROJECT LOGO -->
![logo](img/logo.png)

<h1 align="center">Welcome to search-privacy 🥳🥳🥳</h1>

<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: GPLv3" src="https://img.shields.io/badge/License-GPLv3-yellow.svg" />
  </a>
</p>

### Помочь Донатом на сервак

* 💰 Bitcoin - `1Ebf7gBSSD5Y8y3zLHVy1SuJxAqAgJPjhW`

* 💰 Monero - `45UcKcUN8d6M5cccXZiqC1dtpMsqSazyKSLGP3utQh3MhBmeuCuHAbYBwngsa1eAFs4yza9HukiH4axjNg58YGBrJo2x2tS`

### ✨ [Demo Whoogle](https://whoogle.secven.me/)

### ✨ [Demo SearX](https://search.secven.me/)

#### Установка и обновление Debian
```sh
~$ sudo apt update && sudo apt -y upgrade && sudo apt -y install make git nano
```

#### Редактируем конфиг на ваши данные

```sh
~$ git clone https://git.disroot.org/librewolf/search-privacy.git
~$ cd search-privacy
~$ cp .env.example .env && nano .env
````
* SEARX_DOMAIN=`search.secven.me` заменить на ваш
* WHOOGLE_DOMAIN=`whoogle.secven.me` заменить на ваш
* SERVER_IP=`195.0.0.201` тут айпи тачки

#### Установка Docker compose

```sh
# https://docs.docker.com/engine/install/debian/
# https://docs.docker.com/compose/install/

~$ make install
````

### Install Search-privacy Cloudflare CDN

* [Добавьте ваш домен в днс Cloudflare](./img/dns-cf.png)
* [Настройка SSL/TLS Cloudflare](./img/cf-ssl.png)

```sh
~$ make cf
```

### Install Cloudflare CDN (Custom SSL Server)

* [Добавьте ваш домен в днс Cloudflare](./img/dns-cf.png)
* [Настройка SSL/TLS Cloudflare](./img/ssl-cert.png)
* [Создать сертификат](./img/create-cert.png)
* [Настройка сертификат](./img/ssl-done.png)
> Заменить полученные сертификаты в папке ssl на свои cert.pem и key.pem

```sh
~$ make cfssl
```

### Install default server prod (Let's Encrypt)

```sh
~$ make prod
```

#### Заблокировать censys

```sh
~$ make censys
```

#### Остановить все контейнеры

```sh
~$ make stop
```

### Author

👤 **LibreWolf**

* YouTube - https://www.youtube.com/channel/UCb0MdhCVUA-JeGysDTMjmXg
* Свободный Волк - https://t.me/Libre_Wolf
* Open Source: https://t.me/ThisOpenSource

## Show your support

Give a ⭐️ if this project helped you!

### TODO:
* Tor hidden service
* Fix ModSecurity whoogle  
* Fix push docker hub  
* Anti-DDoS rules cf
* Refactoring


## License

`Distributed under the GPL V3 License. See LICENSE for more information`
